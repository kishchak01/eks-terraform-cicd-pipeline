# Here we are creating 4 subnets, two public and two private subnets, in different AZ.
# We are attaching this subnet to the VPC and providing CIDR block as /19 which will


# Creating private subnet with /19 CIDR which will provide 8192 address and taging the subnet

resource "aws_subnet" "private-us-east-1a" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.private_sn[0]
  availability_zone = var.private_azs[0]

  tags = {
    "Name"                            = "private-us-east-1a"
    "kubernetes.io/role/internal-elb" = "1"
    "kubernetes.io/cluster/demo"      = "owned"
  }
}

# Creating another private subnet with /19 CIDR but the IP subnet is different then the above subnet.

resource "aws_subnet" "private-us-east-1b" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.private_sn[1]
  availability_zone = var.private_azs[1]

  tags = {
    "Name"                            = "private-us-east-1b"
    "kubernetes.io/role/internal-elb" = "1"
    "kubernetes.io/cluster/demo"      = "owned"
  }
}

# Public Subnet 1
resource "aws_subnet" "public-us-east-1a" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.public_sn[0]
  availability_zone       = var.public_azs[0]
  map_public_ip_on_launch = true

  tags = {
    "Name"                       = "public-us-east-1a"
    "kubernetes.io/role/elb"     = "1"
    "kubernetes.io/cluster/demo" = "owned"
  }
}

# Public Subnet 2
resource "aws_subnet" "public-us-east-1b" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.public_sn[1]
  availability_zone       = var.public_azs[1]
  map_public_ip_on_launch = true

  tags = {
    "Name"                       = "public-us-east-1b"
    "kubernetes.io/role/elb"     = "1"
    "kubernetes.io/cluster/demo" = "owned"
  }
}
