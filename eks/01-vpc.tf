# Here we are creating VPC here with the name "main" and providing CIDR block 10.0.0.0/16 so
#it can contain 65535 address in this VPC and providing the tag as main.

resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr

  tags = {
    Name = "main"
  }
}