# We are creating internet GW and attaching to the main vpc.
# It will be used as default route in the public subnet.

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "igw"
  }
}