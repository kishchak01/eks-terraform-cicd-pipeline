variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

variable "private_sn" {
    type = list
    default = ["10.0.0.0/19","10.0.32.0/19"]
}

variable "private_azs" {
  type = list
  default = ["us-east-1a","us-east-1b"]
}


variable "public_sn" {
    type = list
    default = ["10.0.64.0/19","10.0.96.0/19"]
}

variable "public_azs" {
  type = list
  default = ["us-east-1a","us-east-1b"]
}
